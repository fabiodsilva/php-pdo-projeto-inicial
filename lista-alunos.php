<?php

use Alura\Pdo\Domain\Model\Student;
use Alura\Pdo\Infrastructure\Persistence\ConnectionCreator;
use Alura\Pdo\Infrastructure\Repository\PdoStudentRepository;

require_once 'vendor/autoload.php';


$connection=ConnectionCreator::createConnection();
$pdoStudentRepository=new PdoStudentRepository($connection);

var_dump($pdoStudentRepository->allStudents());
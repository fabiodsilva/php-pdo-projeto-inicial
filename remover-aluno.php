<?php

use Alura\Pdo\Infrastructure\Persistence\ConnectionCreator;
use Alura\Pdo\Infrastructure\Repository\PdoStudentRepository;
use Alura\Pdo\Domain\Model\Student;

require_once 'vendor/autoload.php';
$connection=ConnectionCreator::createConnection();
$pdoStudentRepository=new PdoStudentRepository($connection);
$student = new Student(
    3,
    "Fábio",
    new \DateTimeImmutable('1997-10-15')
);


/*$preparedStatement = $pdo->prepare('DELETE FROM students WHERE id = ?;');
$preparedStatement->bindValue(1, 2,PDO::PARAM_INT);
*/

var_dump($pdoStudentRepository->remove($student));
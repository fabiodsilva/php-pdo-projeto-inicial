<?php

use Alura\Pdo\Domain\Model\Student;
use Alura\Pdo\Infrastructure\Persistence\ConnectionCreator;
use Alura\Pdo\Infrastructure\Repository\PdoStudentRepository;


require_once 'vendor/autoload.php';

$student = new Student(
    null,
    "Raquel",
    new \DateTimeImmutable('1997-10-15')
);

$connection=ConnectionCreator::createConnection();
$pdoStudentRepository=new PdoStudentRepository($connection);
if ($pdoStudentRepository->save($student)) {
    echo "Aluno incluído";
}